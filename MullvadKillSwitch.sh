#!/bin/sh
## MullvadKillSwitch.sh


sudo iptables -P OUTPUT DROP
sudo iptables -A OUTPUT -o tun+ -j ACCEPT
sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A OUTPUT -o lo -j ACCEPT
sudo iptables -A OUTPUT -d 255.255.255.255 -j ACCEPT
sudo iptables -A INPUT -s 255.255.255.255 -j ACCEPT
sudo iptables -A OUTPUT -o eth+ -p udp -m multiport --dports 53,1300,1194,1195,1196,1197 -d 185.65.132.0/24,185.65.134.0/24,185.65.135.0/24,193.138.219.0/24 -j ACCEPT

sudo iptables -A OUTPUT -o eth+ -p udp -m multiport --dports 1301,1302 -d 185.65.132.0/24,185.65.134.0/24,185.65.135.0/24,193.138.219.0/24 -j ACCEPT

sudo iptables -A OUTPUT -o eth+ -p tcp -m multiport --dports 53,443 -d 185.65.132.0/24,193.138.219.0/24,185.65.134.0/24,185.65.135.0/24 -j ACCEPT
sudo iptables -A OUTPUT -o eth+ ! -d 193.138.219.228 -p tcp --dport 53 -j DROP

sudo ip6tables -P OUTPUT DROP
sudo ip6tables -A OUTPUT -o tun+ -j ACCEPT
