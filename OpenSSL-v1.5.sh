#!/bin/sh


 *  **PKI**: Public Key Infrastructure. This describes the collection of files
    and associations between the CA, keypairs, requests, and certificates.
 *  **CA**: Certificate Authority. This is the "master cert" at the root of a
    PKI.
 *  **cert**: Certificate. A certificate is a request that has been signed by a
    CA. The certificate contains the public key, some details describing the
    cert itself, and a digital signature from the CA.
 *  **request**: Certificate Request (optionally 'req'.) This is a request for a
    certificate that is then send to a CA for signing. A request contains the
    desired cert information along with a digital signature from the private
    key.
 *  **keypair**: A keypair is an asymmetric cryptographic pair of keys. These
    keys are split into two parts: the public and private keys. The public key
    is included in a request and certificate.




Generate an RSA private key using default parameters:

openssl genpkey -algorithm RSA -out key.pem


Encrypt output private key using 128 bit AES and the passphrase "hello":

openssl genpkey -algorithm RSA -out key.pem -aes-256-cbc -pass pass:hello




pkeyutl
rsautl
x509
genpkey
pkcs7
pkeyutl
gendsa
pkcs12
verify
x509v3_config
ca
req
CA.pl
spkac
config
X509v3
crypto
signver
openssl-verify
openssl-rsautl
openssl-rand




signtool option -G 

generates a new public-private key pair and certificate







Set security level to 2 and display all ciphers consistent with level 2:

openssl ciphers -s -v 'ALL:@SECLEVEL=2'








keyrings

keyutils
persistent-keyring
user-keyring
user-session-keyring
user-namespaces
request-key
credentials





keytool -list -keystore java.home/lib/security/cacerts



keytool -printcert -file 



Print some info about a PKCS#12 file:
openssl pkcs12 -in file.p12 -info -noout -fingerprint


calculate the fingerprint of RiseupCA.pem
certtool -i < RiseupCA.pem |egrep -A 1 'SHA256 fingerprint'



openssl x509 -sha256 -in RiseupCA.pem -noout -fingerprint


head -n -1 RiseupCA.pem | tail -n +2 | base64 -d | sha256sum


sudo openvpn --client --dev tun --auth-user-pass --remote vpn.riseup.net 1194 --keysize 256 --auth SHA256 --cipher AES-256-CBC --ca RiseupCA.pem 





# check site ssl certificate dates
echo | openssl s_client -connect www.google.com:443 2>/dev/null |openssl x509 -dates -noout










 
 
 

sudo openssl req -newkey rsa:4096 -keyout /etc/openvpn/vpn-key.pem -out vpn.csr


sudo openssl req -newkey rsa:4096 -keyout /etc/openvpn/ClientVPNKey.pem -out /etc/openvpn/ClientVPNKey.csr

echo 'OpenVPN' | sha256sum | cut -c1-20

echo 'ClientVPN' | sha256sum | cut -c1-20
echo 'Challenge' | sha256sum | cut -c1-20

sudo openssl req -newkey rsa:4096 -keyout /etc/openvpn/ServerVPNKey.pem -out ServerVPNKey.csr

echo 'ServerVPN' | sha256sum | cut -c1-20
echo 'Challenge' | sha256sum | cut -c1-20

openssl x509 -CA cacert.pem -CAkey cakey.pem -CAcreateserial -days 730 -req -in ClientVPNKey.csr -out ClientVPNKey.pem


Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:

echo -n 'poop' | sha1sum | cut -c1-20



echo "On your CAs environment (hopefully elsewhere):"
openssl x509 -CA cacert.pem -CAkey cakey.pem -CAcreateserial -days 730 -req -in vpn.csr -out vpn-cert.pem



./easy-rsa.sh --batch build-ca nopass
chown nobody:$GROUPNAME /etc/openvpn/crl.pem


sudo openssl dhparam -out /etc/openvpn/dh4096.pem 4096
sudo cp -v dh4096.pem /etc/openvpn/dh2048.pem


sudo openssl dhparam -out /etc/openvpn/ClientVPN-dh4096.pem 4096

sudo openssl dhparam -out /etc/openvpn/ServerVPN-dh4096.pem 4096





openssl x509 -in cert.pem -addtrust clientAuth -setalias "Steve's Class 1 CA" -out trust.pem

genrsa -aes256 -out numbits 512





# Download this file (https://blog.patternsinthevoid.net/isis.txt):
wget -q --ca-certificate=${HOST}.pem https://${HOST}/isis.txt



# Check the SSL certificate fingerprint (it should match the ones given in this file):
cat ${HOME}/${HOST}.pem | openssl x509 -fingerprint -noout -in /dev/stdin


Display the certificate SHA1 fingerprint:
openssl x509 -sha1 -in cert.pem -noout -fingerprint


# Check the SSL certificate fingerprint (it should match the ones given in this file):
cat .pem | openssl x509 -fingerprint -noout -in /dev/stdin


openssl x509 -noout -issuer -subject -fingerprint -dates


openssl s_client -connect  | openssl x509 -text


echo | openssl s_client -connect www.google.com:443 2>/dev/null | openssl x509 -text







echo "##===============================##"
echo "[+] This report that displays 	 "
echo "    The following attributes: 	 "
echo "##===============================##"

echo "#>-------------------------------<#"
echo "    -> certificate issuer			"
echo "#--------------------------------#"
echo "    -> certificate name			"
echo "#--------------------------------#"
echo "    -> fingerprint				"
echo "#--------------------------------#"
echo "    -> dates						"
echo "#--------------------------------#"
echo
echo "#>-------------------------------<#"
echo " [?] in addition to the dates:"
echo "#>-------------------------------<#"
echo | openssl s_client -connect www.google.com:443 2>/dev/null | openssl x509 -noout -issuer -subject -fingerprint -dates










openssl s_client -connect "$1:$2" -CAfile $CACERTS -servername $1 < /dev/null 2> /dev/null
    else
openssl --insecure --print-cert --x509cafile "$CACERTS" "$1" -p "$2"  < /dev/null 2>/dev/null









echo "extract_fingerprints"

# Roughly equivalent to "grep -A1 "SHA-1 fingerprint" | head -n 2 | grep -o '[a-f0-9]{40}'"
certtool -i < $1 | sed -n '/SHA-1 fingerprint/{n;p;q}' | sed 's/\s\+\([a-f0-9]\{40\}\)/\1/'

    else

openssl x509 -in $1 -fingerprint -noout | normalize




# OpenSSL's format: Mar  7 16:08:35 2022 GMT
DATA=$(openssl x509 -enddate -noout -in $1 | cut -d'=' -f2-)

    else

# Certtool's format: Mon Mar 07 16:08:35 UTC 2022
DATA=$(certtool -i < "$1" | sed -e '/Not\sAfter/!d' -e 's/^.*:\s\(.*\)/\1/')




BITS=$(openssl x509 -text -noout -in $1 | sed -e '/Public-Key/!d' \
            -e 's/\s\+Public-Key: (\([0-9]\+\) bit)/\1 bits/')
    else
        BITS=$(certtool -i < $1 | sed -e '/^.*Algorithm Security Level/!d' \
            -e 's/.*(\([0-9]\+\) bits).*/\1 bits/')




get_sigtype() {
    if [ $OPENSSL ]; then
        TYPE=$(openssl x509 -text -noout -in $1 | sed -e '/Signature Algorithm/!d' \
            -e 's/\s\+Signature Algorithm:\s\+\(.\+\)/\1/' | head -n1)
    else
        TYPE=$(certtool -i < $1 | sed -e '/^.*Signature Algorithm:/!d' \
            -e 's/.*:\s\+\(.*\)/\1/')






Sign a SSL Certificate Request (CSR)



#	create an own SSLeay config
cat >ca.config <<EOT
[ ca ]
default_ca			= CA_own
[ CA_own ]
dir				= /usr/share/ssl
certs				= /usr/share/ssl/certs
new_certs_dir			= /usr/share/ssl/ca.db.certs
database			= /usr/share/ssl/ca.db.index
serial				= /usr/share/ssl/ca.db.serial
RANDFILE			= /usr/share/ssl/ca.db.rand
certificate			= /usr/share/ssl/certs/ca.crt
private_key			= /usr/share/ssl/private/ca.key
default_days			= 365
default_crl_days		= 30
default_md			= md5
preserve			= no
policy				= policy_anything
[ policy_anything ]
countryName			= optional
stateOrProvinceName 		= optional
localityName			= optional
organizationName		= optional
organizationalUnitName		= optional
commonName			= supplied
emailAddress			= optional
EOT




# sign the certificate
echo "CA signing: $CSR -> $CERT:"
openssl ca -config ca.config -out $CERT -infiles $CSR

echo "CA verifying: $CERT <-> CA cert"
openssl verify -CAfile /usr/share/ssl/certs/ca.crt $CERT


  # Self-sign
  if [ "$KEYTYPE" == "ssl-self" ]; then
    openssl x509 -in "${NODE}_csr.pem" -out "$NODE.crt" -req -signkey "${NODE}_privatekey.pem" -days 365
    chmod 600 "${NODE}_privatekey.pem"

openssl req -batch -nodes -config openssl.conf -newkey rsa:4096 -sha256 -keyout ${NODE}_privatekey.pem -out ${NODE}_csr.pem

openssl req -noout -text -in ${NODE}_csr.pem








    # Try to get an initial recipient
    if [ -e "$HOME/.gnupg/gpg.conf" ]; then
      recipient="`grep -e "^default-key" ~/.gnupg/gpg.conf | cut -d ' ' -f 2`"


# Check the main key
gpg --with-colons --fixed-list-mode --list-keys "$recipient" | grep ^pub | cut -d : -f 7

  # Check the subkeys
gpg --with-colons --fixed-list-mode --list-keys "$recipient" | grep ^sub | cut -d : -f 7


key="`gpg --fingerprint --with-colons $recipient 2> /dev/null`"


fpr="`echo "$key" | grep -e '^fpr:' | head -1 | cut -d : -f 10`"
uid="`echo "$key" | grep -e '^uid:' | head -1 | cut -d : -f 10 | sed -e 's|^[^<]*<||' -e 's|>$||'`"





  # Show cert fingerprint
  if [ "$KEYTYPE" == "ssl-self" ]; then
    openssl x509 -noout -in "$TMPWORK/${NODE}.crt" -fingerprint


















PKCS#10 certificate request








## Create a private key and then generate a certificate request from it:

openssl genrsa -out key.pem 4096
openssl req -new -key key.pem -out req.pem


## Examine and verify certificate request:
openssl req -in req.pem -text -verify -noout


## The same but just using req:
openssl req -newkey rsa:4096 -keyout key.pem -out req.pem



## Generate a self signed root certificate:
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out req.pem


## Sign a certificate request, using CA extensions:
openssl ca -in req.pem -extensions v3_ca -out newcert.pem


## Sign several requests:
openssl ca -infiles req1.pem req2.pem req3.pem









Create some DSA parameters:

openssl dsaparam -out dsap.pem 1024


Create a DSA CA certificate and private key:

openssl req -x509 -newkey dsa:dsap.pem -keyout cacert.pem -out cacert.pem


Create a DSA certificate request and private key 
(a different set of parameters can optionally be created first):

openssl req -out newreq.pem -newkey dsa:dsap.pem


Sign the request:

CA.pl -signreq





# The following is a standard PKCS1-v1_5 padding for SHA256 signatures, as
# defined in RFC3447. It is prepended to the actual signature (32 bytes) to
# form a sequence of 256 bytes (2048 bits) that is amenable to RSA signing. The
# padded hash will look as follows:
#
#    0x00 0x01 0xff ... 0xff 0x00  ASN1HEADER  SHA256HASH
#   |--------------205-----------||----19----||----32----|
#
# where ASN1HEADER is the ASN.1 description of the signed data. The complete 51
# bytes of actual data (i.e. the ASN.1 header complete with the hash) are
# packed as follows:
#
#  SEQUENCE(2+49) {
#   SEQUENCE(2+13) {
#    OBJECT(2+9) id-sha256
#    NULL(2+0)
#   }
#   OCTET STRING(2+32) <actual signature bytes...>
#  }






Parse a PKCS#12 file and output it to a file:

openssl pkcs12 -in file.p12 -out file.pem


Print some info about a PKCS#12 file:
openssl pkcs12 -in file.p12 -info -noout



Create a PKCS#12 file:

openssl pkcs12 -export -in file.pem -out file.p12 -name "My Certificate"


Include some extra certificates:

openssl pkcs12 -export -in file.pem -out file.p12 -name "My Certificate" -certfile othercerts.pem















Generate a CRL
openssl ca -gencrl -out crl.pem










CA.pl -newca
CA.pl -newreq
CA.pl -signreq
CA.pl -pkcs12 "My Test Certificate"



-newcert				## 
-newreq				## 
-newca				## 
-pkcs12				## 
-crl				## 
				## 




-sign				## 
				## 
				## 
				## 
				## 
				## 
				## 
				## 
openssl req				## 
openssl pkcs12				## 
openssl ca				## 
openssl x509				## 

openssl verify




# padding for openssl rsautl -pkcs (smartcard keys)
#
# The following is an ASN.1 header. It is prepended to the actual signature
# (32 bytes) to form a sequence of 51 bytes. OpenSSL will add additional
# PKCS#1 1.5 padding during the signing operation. The padded hash will look
# as follows:
#
#    ASN1HEADER  SHA256HASH
#   |----19----||----32----|
#
# where ASN1HEADER is the ASN.1 description of the signed data. The complete 51
# bytes of actual data (i.e. the ASN.1 header complete with the hash) are
# packed as follows:
#
#  SEQUENCE(2+49) {
#   SEQUENCE(2+13) {
#    OBJECT(2+9) id-sha256
#    NULL(2+0)
#   }
#   OCTET STRING(2+32) <actual signature bytes...>
#  }


-in
-out
-text
-new
-pubkey
-verify

-newkey rsa



openssl rsautl -verify -in file -inkey ClientVPNKey.pem -raw -hexdump
openssl rsautl -verify -in sig -inkey ClientVPNKey.pem


openssl rsautl -sign -in file -inkey key.pem -out sig
openssl req -nodes -new -x509 -keyout ca.key -out ca.crt --genkey --secret xe1phix.key





openssl genrsa -out "${OUT}.key" 4096


openssl req -new -key "${OUT}.key" -out "${OUT}.csr" -subj '/C=US/ST=CA/L=San Francisco/O=Docker/CN=Notary Testing Client Auth'

openssl x509 -req -days 3650 -in "${OUT}.csr" -signkey "${OUT}.key" -out "${OUT}.crt" -extfile "${OUT}.cnf" -extensions ssl_client





openssl genrsa -out "${OUT}.key" 4096

openssl req -new -nodes -key "${OUT}.key" -out "${OUT}.csr" -subj "/C=US/ST=CA/L=San Francisco/O=Docker/CN=${COMMONNAME}" -config "${OUT}.cnf" -extensions "v3_req"

openssl x509 -req -days 3650 -in "${OUT}.csr" -signkey "${OUT}.key" -out "${OUT}.crt" -extensions v3_req -extfile "${OUT}.cnf"









echo "## ================================================ ##"
echo "## 		[+] Generate key for tls-auth				"
echo "## ================================================ ##"
openvpn --genkey --secret /etc/openvpn/ta.key


In the server configuration, add:

tls-auth ta.key 0

In the client configuration, add:

tls-auth ta.key 1












Convert a certificate from PEM to DER format:

openssl x509 -in cert.pem -inform PEM -out cert.der -outform DER









echo "## ================================================ ##"
echo "## [+] Change The Permissions to Private Files:		"
echo "## ================================================ ##"
Change The Permissions to Private Files:
sudo chmod 600 /etc/openvpn/vpn-key.pem
sudo chmod 600 /etc/openvpn/ta.key



echo "## ========================================================= ##"
echo "## [+] Turn on The Immutable Bit For The VPN Keys & Certs:	 " 
echo "## ========================================================= ##"
chattr +i /etc/openvpn/mullvad_ca.crt
chmod -v 0644 
chown -v 
chattr +i /etc/openvpn/mullvad_crl.pem


sudo chmod u+r vpn-key.pem
chattr +i 
sudo chmod ug+r mullvad_userpass.txt
chattr +i ta.key

vpn.csr

chattr +i dh4096.pem



echo "## ================================================ ##"
echo "				[+] TLS Authentication					"
echo "## ================================================ ##"
echo "## ------------------------------------------------ ##"
echo "## [?] To enable TLS authentication					"
echo "## 	 first generate a static encryption key. 		"
echo "## 	 This needs to be securely copied 				"
echo "## 	 to all OpenVPN clients and servers.			"
echo "## ------------------------------------------------ ##"
echo "## ================================================ ##"
openvpn --genkey --secret vpn.tlsauth


echo "## ================================================================ ##"
echo "## 					In the configuration files: 					"
echo "## ================================================================ ##"
echo "## ---------------------------------------------------------------- ##"
echo "## [?] The KEYDIR must be 0 on one of the sides and 1 on the other. 	"
echo "## 	 So if you choose the KEYDIR value of 0 for the server, all		"
echo "## 	 clients must be 1, and vice versa.								"
echo "## ---------------------------------------------------------------- ##"
echo "## ================================================================ ##"
tls-auth myvpn.tlsauth 



./easyrsa help options

	# Configure vars
	sed -i "s/KEY_SIZE=.*/KEY_SIZE=4096/g" /etc/openvpn/easy-rsa/vars
	sed -i 's/export CA_EXPIRE=3650/export CA_EXPIRE=365/' /etc/openvpn/easy-rsa/vars
	sed -i 's/export KEY_EXPIRE=3650/export KEY_EXPIRE=365/' /etc/openvpn/easy-rsa/vars
	sed -i "s/export KEY_COUNTRY=\"US\"/export KEY_COUNTRY=\"$country\"/" /etc/openvpn/easy-rsa/vars
	sed -i "s/export KEY_PROVINCE=\"CA\"/export KEY_PROVINCE=\"$province\"/" /etc/openvpn/easy-rsa/vars
	sed -i "s/export KEY_CITY=\"SanFrancisco\"/export KEY_CITY=\"$city\"/" /etc/openvpn/easy-rsa/vars
	sed -i "s/export KEY_ORG=\"Fort-Funston\"/export KEY_ORG=\"$organization\"/" /etc/openvpn/easy-rsa/vars
	sed -i "s/export KEY_EMAIL=\"me@myhost.mydomain\"/export KEY_EMAIL=\"$email\"/" /etc/openvpn/easy-rsa/vars
	sed -i "s/export KEY_OU=\"MyOrganizationalUnit\"/export KEY_OU=\"$organizationUnit\"/" /etc/openvpn/easy-rsa/vars
	sed -i "s/export KEY_NAME=\"EasyRSA\"/export KEY_NAME=\"$commonName\"/" /etc/openvpn/easy-rsa/vars
	sed -i "s/export KEY_CN=openvpn.example.com/export KEY_CN=\"$commonName\"/" /etc/openvpn/easy-rsa/vars


./easyrsa init-pki
sudo ./easyrsa build-ca nopass
sudo ./easyrsa gen-req VPN.csr nopass

req: /etc/openvpn/easy-rsa/easyrsa3/pki/reqs/VPN.csr.req
key: /etc/openvpn/easy-rsa/easyrsa3/pki/private/VPN.csr.key

sudo ./easyrsa build-client-full Xe1phix

echo 'Xe1phix.key' | sha256sum | cut -c1-20


sudo ./easyrsa gen-crl
CRL file: /etc/openvpn/easy-rsa/easyrsa3/pki/crl.pem

set-rsa-pass 


openssl ocsp -issuer "$issuer" "$nonce" -CAfile "$verify" -url "$ocsp_url" -serial "${serial}"




# Certificate Authority
openssl genrsa 4096																				## ca-key.pem
openssl req -sha256 -new -key ca-key.pem -subj /CN=OpenVPN-CA/									## ca-csr.pem 
openssl x509 -req -sha256 -in ca-csr.pem -signkey ca-key.pem -days 365							## ca-cert.pem
echo 01																							## ca-cert.srl


# Server Key & Certificate
openssl genrsa 4096																				## server-key.pem
openssl req -sha256 -new -key server-key.pem -subj /CN=OpenVPN-Server/							## server-csr.pem
openssl x509 -sha256 -req -in server-csr.pem -CA ca-cert.pem -CAkey ca-key.pem -days 365		## server-cert.pem


# Client Key & Certificate
openssl genrsa 4096																				## client-key.pem
openssl req -sha256 -new -key client-key.pem -subj /CN=OpenVPN-Client/							## client-csr.pem
openssl x509 -req -sha256 -in client-csr.pem -CA ca-cert.pem -CAkey ca-key.pem -days 365		## client-cert.pem


# Diffie hellman parameters
openssl dhparam 2048																			## dh.pem 





openssl x509 -text -in certif.crt -noout 			## Read a certificate
openssl req -text -in request.csr -noout  			## Read a Certificate Signing Request


## Generate a Certificate Signing Request (in PEM format) for the public key of a key pair
openssl req -new -key private.key -out request.csr  			


## Create a 2048-bit RSA key pair and generate a Certificate Signing Request for it
openssl req -new -nodes -keyout private.key -out request.csr -newkey rsa:2048 


## Generate a self-signed root certificate (and create a new CA private key)
openssl req -x509 -newkey rsa:2048 -nodes -keyout private.key -out certif.crt -days validity 

## Generate a self-signed certificate
openssl ca -config ca.conf -in request.csr -out certif.crt -days validity -verbose 

## Revoke a certificate
openssl ca -config ca.conf -gencrl -revoke certif.crt -crl_reason why 

## Generate a Certificate Revocation List containing all revoked certificates so far
openssl ca -config ca.conf -gencrl -out crlist.crl 

## Convert a certificate from PEM to DER
openssl x509 -in certif.pem -outform DER -out certif.der 

## Convert a certificate from PEM to PKCS#12 including the private key
openssl pkcs12 -export -in certif.pem -inkey private.key -out certif.pfx -name friendlyname 

openssl pkcs12 -export -inkey keys/bugs.key -in keys/bugs.crt -certfile keys/ca.crt -out keys/bugs.p12

## Create a PEM certificate from CRT and private key
cat cert.crt cert.key > cert.pem 

## Generate the digest of a file
openssl dgst -hashfunction -out file.hash file 

## Verify the digest of a file (no output means that digest verification is successful)
openssl dgst -hashfunction file | cmp -b file.hash 

## Generate the signature of a file
openssl dgst -hashfunction -sign private.key -out file.sig file 


## Verify the signature of a file
openssl dgst -hashfunction -verify public.key -signature file.sig file 

## Encrypt a file
openssl enc -e -cipher -in file -out file.enc -salt 

## Decrypt a file
openssl enc -d -cipher -in file.enc -out file 

## Generate a 2048-bit RSA key pair protected by TripleDES passphrase
openssl genpkey -algorithm RSA -cipher 3des -pkeyopt rsa_keygen_bits:2048 -out key.pem 

## Examine a private key
openssl pkey -text -in private.key -noout 

## Change the passphrase of a private key
openssl pkey -in old.key -out new.key -cipher 

## Remove the passphrase from a private key
openssl pkey -in old.key -out new.key 


## Retrieve and inspect a SSL certificate from a website
openssl s_client -connect www.website.com:443 > tmpfile 
openssl x509 -in tmpfile -text

## List all available hash functions
openssl list-message-digest-commands 

## List all available ciphers
openssl list-cipher-commands 





# Generate CA key and cert
openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 \
    -extensions easyrsa_ca -keyout sample-ca/ca.key -out sample-ca/ca.crt \
    -subj "/C=KG/ST=NA/L=BISHKEK/O=OpenVPN-TEST/emailAddress=me@myhost.mydomain" \
    -config openssl.cnf


# Create server key and cert
openssl req -new -nodes -config openssl.cnf -extensions server \
    -keyout sample-ca/server.key -out sample-ca/server.csr \
    -subj "/C=KG/ST=NA/O=OpenVPN-TEST/CN=Test-Server/emailAddress=me@myhost.mydomain"
    
openssl ca -batch -config openssl.cnf -extensions server -out sample-ca/server.crt -in sample-ca/server.csr


# Create client key and cert
openssl req -new -nodes -config openssl.cnf \
    -keyout sample-ca/client.key -out sample-ca/client.csr \
    -subj "/C=KG/ST=NA/O=OpenVPN-TEST/CN=Test-Client/emailAddress=me@myhost.mydomain"

openssl ca -batch -config openssl.cnf -out sample-ca/client.crt -in sample-ca/client.csr



# Create password protected key file
openssl rsa -aes256 -passout pass:password -in sample-ca/client.key -out sample-ca/client-pass.key


# Create pkcs#12 client bundle
openssl pkcs12 -export -nodes -password pass:password \
    -out sample-ca/client.p12 -inkey sample-ca/client.key \
    -in sample-ca/client.crt -certfile sample-ca/ca.crt

# Create a client cert, revoke it, generate CRL
openssl req -new -nodes -config openssl.cnf \
    -keyout sample-ca/client-revoked.key -out sample-ca/client-revoked.csr \
    -subj "/C=KG/ST=NA/O=OpenVPN-TEST/CN=client-revoked/emailAddress=me@myhost.mydomain"
openssl ca -batch -config openssl.cnf -out sample-ca/client-revoked.crt -in sample-ca/client-revoked.csr
openssl ca -config openssl.cnf -revoke sample-ca/client-revoked.crt
openssl ca -config openssl.cnf -gencrl -out sample-ca/ca.crl


# Create DSA server and client cert (signed by 'regular' RSA CA)
openssl dsaparam -out sample-ca/dsaparams.pem 2048

openssl req -new -newkey dsa:sample-ca/dsaparams.pem -nodes -config openssl.cnf \
    -extensions server \
    -keyout sample-ca/server-dsa.key -out sample-ca/server-dsa.csr \
    -subj "/C=KG/ST=NA/O=OpenVPN-TEST/CN=Test-Server-DSA/emailAddress=me@myhost.mydomain"
openssl ca -batch -config openssl.cnf -extensions server \
    -out sample-ca/server-dsa.crt -in sample-ca/server-dsa.csr

openssl req -new -newkey dsa:sample-ca/dsaparams.pem -nodes -config openssl.cnf \
    -keyout sample-ca/client-dsa.key -out sample-ca/client-dsa.csr \
    -subj "/C=KG/ST=NA/O=OpenVPN-TEST/CN=Test-Client-DSA/emailAddress=me@myhost.mydomain"
openssl ca -batch -config openssl.cnf \
    -out sample-ca/client-dsa.crt -in sample-ca/client-dsa.csr



# Create EC server and client cert (signed by 'regular' RSA CA)

openssl ecparam -out sample-ca/secp256k1.pem -name secp256k1

openssl req -new -newkey ec:sample-ca/secp256k1.pem -nodes -config openssl.cnf \
    -extensions server \
    -keyout sample-ca/server-ec.key -out sample-ca/server-ec.csr \
    -subj "/C=KG/ST=NA/O=OpenVPN-TEST/CN=Test-Server-EC/emailAddress=me@myhost.mydomain"
    
openssl ca -batch -config openssl.cnf -extensions server -out sample-ca/server-ec.crt -in sample-ca/server-ec.csr


openssl req -new -newkey ec:sample-ca/secp256k1.pem -nodes -config openssl.cnf \
    -keyout sample-ca/client-ec.key -out sample-ca/client-ec.csr \
    -subj "/C=KG/ST=NA/O=OpenVPN-TEST/CN=Test-Client-EC/emailAddress=me@myhost.mydomain"
    
openssl ca -batch -config openssl.cnf -out sample-ca/client-ec.crt -in sample-ca/client-ec.csr





















How to verify this file and the server it lives on:
---------------------------------------------------
# Download the certificate for this server:
HOST=blog.patternsinthevoid.net && PORT=443
openssl s_client -connect "${HOST}":"${PORT}" </dev/null 2>/dev/null | \
    sed -nr '/(-){5}(BEGIN CERTIFICATE){1}(-){5}/,/(-){5}(END CERTIFICATE){1}(-){5}/p' | \
    cat > ${HOME}/${HOST}.pem
# Check the SSL certificate fingerprint (it should match the ones given in this file):
cat ${HOME}/${HOST}.pem | openssl x509 -fingerprint -noout -in /dev/stdin
# Download this file (https://blog.patternsinthevoid.net/isis.txt):
wget -q --ca-certificate=${HOST}.pem https://${HOST}/isis.txt
# Check signature and import key:
gpg -o isis -v isis.txt && gpg --import isis












